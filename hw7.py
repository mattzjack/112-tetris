#################################################
# hw7.py: Tetris!
#
# Your name: Matt Kong
# Your andrew id: ymkong
# date: Oct 12, 2019
#
# Your partner's name:
# Your partner's andrew id:
#################################################

import cs112_f19_week7_linter
import math, copy, random

from cmu_112_graphics import *
from tkinter import *

#################################################
# Helper functions
#################################################

def almostEqual(d1, d2, epsilon=10**-7):
    # note: use math.isclose() outside 15-112 with Python version 3.5 or later
    return (abs(d2 - d1) < epsilon)

import decimal
def roundHalfUp(d):
    # Round to nearest with ties going away from zero.
    rounding = decimal.ROUND_HALF_UP
    # See other rounding options here:
    # https://docs.python.org/3/library/decimal.html#rounding-modes
    return int(decimal.Decimal(d).to_integral_value(rounding=rounding))

#################################################
# Functions for you to write
#################################################

##### helper #####

def getLeftTopRightBot(app, r, c):
    x0 = app.margin + c * app.cellSideLen
    x1 = x0 + app.cellSideLen
    y0 = app.margin + r * app.cellSideLen
    y1 = y0 + app.cellSideLen
    return (x0, y0, x1, y1)

##### app started #####

def appStarted(app):
    # constants
    app.framerate = 30 # Hz
    app.timerDelay = 1000 // app.framerate # ms
    app.backgroundColor = 'orange'
    app.emptyColor = 'blue'
    app.cellWidth = 3
    (app.m, app.n, app.cellSideLen, app.margin) = gameDimensions()
    app.tetrisPieces = (
        # I
        (( True,  True,  True,  True),),
        # J
        (( True,  True,  True),
         (False, False,  True)),
        # L
        ((False, False,  True),
         ( True,  True,  True)),
        # O
        (( True,  True),
         ( True,  True)),
        # S
        ((False,  True,  True),
         ( True,  True, False)),
        # T
        ((False,  True, False),
         ( True,  True,  True)),
        # Z
        (( True,  True, False),
         (False,  True,  True))
    )
    app.tetrisPieceColors = ('red', 'yellow', 'magenta', 'pink', 'cyan',
                             'green', 'orange')
    # init others
    app.board = [[app.emptyColor] * app.n for _ in range(app.m)]
    # falling piece
    app.fallingPiece = None
    app.fallingPieceTopRow = None
    app.fallingPieceLeftCol = None
    app.fallingPieceColor = None
    # book-keeping
    app.over = False
    app.framecount = 0
    app.timerTick = 0.5 # s
    app.nframesPerTick = int(app.timerTick * app.framerate)
    app.score = 0

##### controller helpers #####

def fallingPieceIsLegal(app):
    for dr in range(len(app.fallingPiece)):
        for dc in range(len(app.fallingPiece[0])):
            if app.fallingPiece[dr][dc]:
                r = app.fallingPieceTopRow + dr
                c = app.fallingPieceLeftCol + dc
                if not (0 <= r < app.m and 0 <= c < app.n and
                        app.board[r][c] == app.emptyColor):
                    return False
    return True

def moveFallingPiece(app, direction):
    if app.fallingPiece == None:
        return
    if direction == 'Down':
        (dr, dc) = (1, 0)
    elif direction == 'Left':
        (dr, dc) = (0, -1)
    else: # direction = right
        (dr, dc) = (0, 1)
    app.fallingPieceTopRow += dr
    app.fallingPieceLeftCol += dc
    if not fallingPieceIsLegal(app):
        app.fallingPieceTopRow -= dr
        app.fallingPieceLeftCol -= dc
        return False
    return True

def rotate(t2d):
    r0 = len(t2d)
    c0 = len(t2d[0])
    (r1, c1) = (c0, r0)
    res = [[None] * c1 for _ in range(r1)]
    for i in range(r1):
        for j in range(c1):
            res[i][j] = t2d[j][c0 - i - 1]
        res[i] = tuple(res[i])
    return tuple(res)

def rotateFallingPiece(app):
    if app.fallingPiece == None:
        return
    oldTR = app.fallingPieceTopRow
    oldLC = app.fallingPieceLeftCol
    oldPRows = len(app.fallingPiece)
    oldPCols = len(app.fallingPiece[0])
    (newPRows, newPCols) = (oldPCols, oldPRows)
    app.fallingPieceTopRow = oldTR + oldPRows // 2 - newPRows // 2
    app.fallingPieceLeftCol = oldLC + oldPCols // 2 - newPCols // 2
    app.fallingPiece = rotate(app.fallingPiece)
    if not fallingPieceIsLegal(app):
        app.fallingPieceTopRow = oldTR
        app.fallingPieceLeftCol = oldLC
        for _ in range(3):
            app.fallingPiece = rotate(app.fallingPiece)

def genNewFallingPiece(app):
    app.fallingPiece = random.choice(app.tetrisPieces)
    app.fallingPieceTopRow = 0
    app.fallingPieceLeftCol = app.n // 2 - len(app.fallingPiece[0]) // 2
    app.fallingPieceColor = random.choice(app.tetrisPieceColors)
    if not fallingPieceIsLegal(app):
        app.over = True

def placeFallingPiece(app):
    for dr in range(len(app.fallingPiece)):
        for dc in range(len(app.fallingPiece[0])):
            if app.fallingPiece[dr][dc]:
                r = app.fallingPieceTopRow + dr
                c = app.fallingPieceLeftCol+ dc
                app.board[r][c] = app.fallingPieceColor
    app.fallingPiece = None

def removeFullRows(app):
    new = [r for r in app.board if app.emptyColor in r]
    nremoved = app.m - len(new)
    app.board = [[app.emptyColor] * app.n for _ in range(nremoved)] + new
    app.score += nremoved ** 2

def hardDrop(app):
    while moveFallingPiece(app, 'Down'):
        pass
    placeFallingPiece(app)
    removeFullRows(app)
    genNewFallingPiece(app)

##### controllers #####

def keyPressed(app, event):
    if app.over:
        return
    if event.key in ['Down', 'Left', 'Right']:
        moveFallingPiece(app, event.key)
    elif event.key == 'Up':
        rotateFallingPiece(app)
    elif event.key == 'Space':
        hardDrop(app)

def timerFired(app):
    if app.over:
        return
    app.framecount = (app.framecount + 1) % app.nframesPerTick
    if app.framecount == 0:
        if app.fallingPiece == None:
            genNewFallingPiece(app)
        else:
            success = moveFallingPiece(app, 'Down')
            if not success:
                # should also reset fallingPiece to None
                placeFallingPiece(app)
                removeFullRows(app)

##### redraw all #####

def drawBackground(app, canvas):
    canvas.create_rectangle(-10, -10, app.width + 10, app.height + 10,
                            fill=app.backgroundColor, width=0)

def drawCell(app, canvas, r, c, fill):
    canvas.create_rectangle(getLeftTopRightBot(app, r, c),
                            fill=fill, width=app.cellWidth)

def drawBoard(app, canvas):
    for r in range(app.m):
        for c in range(app.n):
            drawCell(app, canvas, r, c, app.board[r][c])

def drawFallingPiece(app, canvas):
    r0 = app.fallingPieceTopRow
    c0 = app.fallingPieceLeftCol
    for dr in range(len(app.fallingPiece)):
        for dc in range(len(app.fallingPiece[0])):
            if app.fallingPiece[dr][dc]:
                drawCell(app, canvas, r0 + dr, c0 + dc, app.fallingPieceColor)

def drawOver(app, canvas):
    canvas.create_rectangle(0, app.margin, app.width, app.margin * 3,
                            fill='black', width=app.cellWidth)
    canvas.create_text(app.width / 2, app.margin * 2, fill='white',
                       text='You Lost!', font=f'Consolas {app.margin}')

def drawScore(app, canvas):
    canvas.create_text(app.width / 2, app.margin / 2,
                       text=f'score: {app.score}',
                       font=f'Consolas {app.margin // 2} bold')

def redrawAll(app, canvas):
    drawBackground(app, canvas)
    drawBoard(app, canvas)
    drawScore(app, canvas)
    if app.fallingPiece != None:
        drawFallingPiece(app, canvas)
    if app.over:
        drawOver(app, canvas)

##### main #####

def gameDimensions():
    # nrows, ncols, cellSideLen (px), margin (px)
    return (15, 10, 20, 25)

def playTetris():
    (m, n, cellSideLen, margin) = gameDimensions()
    width = n * cellSideLen + 2 * margin
    height = m * cellSideLen + 2 * margin
    runApp(width=width, height=height)

#################################################
# main
#################################################

def main():
    cs112_f19_week7_linter.lint()
    playTetris()

if __name__ == '__main__':
    main()
